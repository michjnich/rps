module Game exposing
    ( Game(..)
    , GameOption(..)
    , GameResult(..)
    , decodeGame
    , decodeGameResult
    , encodeGame
    , encodeGameOption
    , enumGame
    , gameDescription
    , gameOptionDescription
    , getGameOptions
    )


type Game
    = RoPaSc
    | RoPaScLiSp


enumGame : List Game
enumGame =
    [ RoPaSc
    , RoPaScLiSp
    ]


type GameOption
    = Rock
    | Paper
    | Scissors
    | Lizard
    | Spock


type GameResult
    = Win
    | Lose
    | Tie


getGameOptions : Game -> List GameOption
getGameOptions game =
    case game of
        RoPaSc ->
            [ Rock, Paper, Scissors ]

        RoPaScLiSp ->
            [ Rock, Paper, Scissors, Lizard, Spock ]


gameDescription : Game -> String
gameDescription game =
    case game of
        RoPaSc ->
            "Rock, Paper, Scissors"

        RoPaScLiSp ->
            "Rock, Paper, Scissors, Lizard, Spock"


gameOptionDescription : Maybe GameOption -> String
gameOptionDescription gameOption =
    case gameOption of
        Just Rock ->
            "Rock"

        Just Paper ->
            "Paper"

        Just Scissors ->
            "Scissors"

        Just Lizard ->
            "Lizard"

        Just Spock ->
            "Spock"

        Nothing ->
            ""


decodeGame : String -> Maybe Game
decodeGame gameString =
    let
        gs =
            gameString |> String.trim |> String.toUpper
    in
    case gs of
        "RPS" ->
            Just RoPaSc

        "RPSLS" ->
            Just RoPaScLiSp

        _ ->
            Nothing


encodeGame : Maybe Game -> String
encodeGame game =
    case game of
        Just RoPaSc ->
            "RPS"

        Just RoPaScLiSp ->
            "RPSLS"

        Nothing ->
            ""


decodeGameOption : String -> Maybe GameOption
decodeGameOption gameOptionString =
    let
        gos =
            gameOptionString |> String.trim |> String.toUpper
    in
    case gos of
        "ROCK" ->
            Just Rock

        "PAPER" ->
            Just Paper

        "SCISSORS" ->
            Just Scissors

        "LIZARD" ->
            Just Lizard

        "SPOCK" ->
            Just Spock

        _ ->
            Nothing


encodeGameOption : Maybe GameOption -> String
encodeGameOption gameOption =
    case gameOption of
        Just Rock ->
            "ROCK"

        Just Paper ->
            "PAPER"

        Just Scissors ->
            "SCISSORS"

        Just Lizard ->
            "LIZARD"

        Just Spock ->
            "SPOCK"

        Nothing ->
            ""


decodeGameResult : String -> ( Maybe GameResult, Maybe GameOption )
decodeGameResult resultStr =
    let
        res =
            case resultStr |> String.slice 0 1 of
                "W" ->
                    Just Win

                "L" ->
                    Just Lose

                "T" ->
                    Just Tie

                _ ->
                    Nothing

        oChoice =
            resultStr |> String.slice 1 -1 |> decodeGameOption
    in
    ( res, oChoice )
