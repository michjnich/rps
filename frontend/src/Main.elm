module Main exposing (Model, Msg(..), main)

import Browser exposing (Document)
import Debug
import Game
    exposing
        ( Game(..)
        , GameOption(..)
        , GameResult(..)
        , decodeGame
        , decodeGameResult
        , encodeGame
        , encodeGameOption
        , enumGame
        , gameDescription
        , gameOptionDescription
        , getGameOptions
        )
import Html exposing (Html, button, div, h1, h2, text)
import Html.Events exposing (onClick)
import Json.Decode as D
import Ports exposing (receiveMessage, sendMessage)


type alias Model =
    { game : Game
    , options : List GameOption
    , currentSelection : Maybe GameOption
    , lastResult : Maybe GameResult
    , lastOpponentChoice : Maybe GameOption
    }


type Msg
    = SwapGame (Maybe Game)
    | MakeChoice (Maybe GameOption)
    | GetResult (Maybe GameResult) (Maybe GameOption)
    | ServerResponse String
    | NoOp


main : Program () Model Msg
main =
    Browser.document
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }


init : () -> ( Model, Cmd Msg )
init () =
    let
        newGame =
            RoPaSc
    in
    ( { game = newGame
      , options = getGameOptions newGame
      , currentSelection = Nothing
      , lastResult = Nothing
      , lastOpponentChoice = Nothing
      }
    , Cmd.none
    )


gameOptionDisplay : GameOption -> Html Msg
gameOptionDisplay option =
    button
        [ onClick (MakeChoice (Just option)) ]
        [ text (gameOptionDescription (Just option)) ]


gameSelectionDisplay : Game -> Html Msg
gameSelectionDisplay game =
    button
        [ onClick (SwapGame (Just game)) ]
        [ text (gameDescription game) ]


view : Model -> Document Msg
view model =
    let
        gameDesc =
            model.game |> gameDescription
    in
    { title = gameDesc
    , body =
        [ h1 [] [ text gameDesc ]
        , h2 [] [ text "Select game" ]
        , div [] (List.map gameSelectionDisplay enumGame)
        , h2 [] [ text "Make your selection" ]
        , div [] []
        , div [] (List.map gameOptionDisplay model.options)
        , div [] [ text ("Current selection: " ++ gameOptionDescription model.currentSelection) ]
        ]
    }


formatSendMessage : Msg -> String -> String
formatSendMessage m t =
    let
        tLen =
            t |> String.length |> String.fromInt |> String.padRight 3 ' '

        tType =
            case m of
                MakeChoice _ ->
                    "CHOICE"

                SwapGame _ ->
                    "GAME"

                _ ->
                    "NONE"
    in
    tLen ++ tType ++ t


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        MakeChoice option ->
            ( { model | currentSelection = option }
            , encodeGameOption option
                |> formatSendMessage msg
                |> sendMessage
            )

        GetResult res oChoice ->
            ( { model | lastResult = res, lastOpponentChoice = oChoice }
            , Cmd.none
            )

        SwapGame game ->
            let
                g =
                    game
                        |> Maybe.withDefault RoPaSc
            in
            ( { model | game = g, options = getGameOptions g }
            , encodeGame game
                |> formatSendMessage msg
                |> sendMessage
            )

        ServerResponse response ->
            let
                rmsg =
                    decodeServerResponse response
            in
            update rmsg model

        NoOp ->
            ( model, Cmd.none )


subscriptions : Model -> Sub Msg
subscriptions _ =
    receiveMessage ServerResponse


decodeServerResponse : String -> Msg
decodeServerResponse responseVal =
    let
        msg =
            case D.decodeString D.string responseVal of
                Ok val ->
                    val

                Err _ ->
                    ""

        rType =
            msg
                |> String.slice 3 11

        rStr =
            msg
                |> String.slice 11 -1

        _ =
            Debug.log (":" ++ msg ++ ":" ++ rType ++ ":" ++ rStr ++ ":") 1
    in
    case rType of
        "END" ->
            NoOp

        "RESP" ->
            let
                ( res, oChoice ) =
                    decodeGameResult rStr
            in
            GetResult res oChoice

        "GAME" ->
            SwapGame (decodeGame rStr)

        _ ->
            NoOp
