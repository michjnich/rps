# Rock, Paper, Scissors

To run locally:
- `python rps_server.py` in one terminal
- As many `python rps_client.py` processes as you like.

To run on different servers:
- Set env var `RPS_HOST` on both servers to the IP of the RPS server. Default is local host.

To define the port:
- Set env var `RPS_PORT` on both RPS servers to the desired port number. Default is 8888 if not set.

Elm build: 
- From `frontend` dir run `elm make src/Main.elm --output=js/main.js`