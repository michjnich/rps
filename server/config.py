SELECTIONS = {
    "R": "ROCK",
    "ROCK": "ROCK",
    "P": "PAPER",
    "PAPER": "PAPER",
    "S": "SCISSORS",
    "SCISSORS": "SCISSORS",
    "L": "LIZARD",
    "LIZARD": "LIZARD",
    "K": "SPOCK",
    "SPOCK": "SPOCK",
}

RPS = ("ROCK", "PAPER", "SCISSORS")
RPSLS = ("ROCK", "PAPER", "SCISSORS", "LIZARD", "SPOCK")

RPS_WINS = {
    ("PAPER", "ROCK"): "covers",
    ("SCISSORS", "PAPER"): "cuts",
    ("ROCK", "SCISSORS"): "crushes",
}
RPSLS_WINS = {
    **RPS_WINS,
    ("PAPER", "SPOCK"): "disproves",
    ("SCISSORS", "LIZARD"): "decapitates",
    ("ROCK", "LIZARD"): "crushes",
    ("LIZARD", "SPOCK"): "poisons",
    ("LIZARD", "PAPER"): "eats",
    ("SPOCK", "SCISSORS"): "smashes",
    ("SPOCK", "ROCK"): "vapourises",
}
