import os
import socket
import sys
import threading
from random import choice

from config import RPS, RPSLS, RPS_WINS, RPSLS_WINS, SELECTIONS


HOST = os.getenv("RPS_HOST", "localhost")
PORT = int(os.getenv("RPS_PORT", "8888"))


def send_reply(conn, type="RESP", msg=""):
    """
    Format and send replies
    """
    conn.sendall(f"{len(msg):<3}{type:<8}{msg}".encode())


def client_thread(conn, interactive=True):
    """
    Process the client selections
    """
    OPTIONS = RPS
    WINS = RPS_WINS

    while True:
        data_length = int(conn.recv(3).decode())
        msg_type = conn.recv(8).decode().strip()

        if msg_type == "EXIT":
            print("Client disconnected")
            break

        data = conn.recv(data_length).decode() if data_length > 0 else ""
        print(f"Rec:{data_length}:{msg_type}:{data}:")

        reply = []
        if msg_type == "CHOICE":
            p_choice = None
            p_choice = SELECTIONS.get(data.upper(), None)
            if p_choice is None or p_choice not in OPTIONS:
                reply.append("Invalid selection, try again - R'ock, 'P'aper or 'S'cissors\n")
            else:
                c_choice = choice(OPTIONS)
                reply.append(f"You selected {p_choice}")
                reply.append(f"Computer selected {c_choice}")
                if p_choice == c_choice:
                    msg = "Result: Tie"
                elif (p_choice, c_choice) in WINS:
                    reply.append(f"{p_choice} {WINS[(p_choice, c_choice)]} {c_choice}")
                    msg = "Result: You win"
                else:
                    reply.append(f"{c_choice} {WINS[(c_choice, p_choice)]} {p_choice}")
                    msg = "Result: You lose"

                msg = msg + "\n"
                reply.append(msg)

        elif msg_type == "GAME":
            if data.upper() == "RPS":
                OPTIONS = RPS
                WINS = RPS_WINS
                reply.append("You are now playing Rock, Paper, Scissors/n")
            elif data.upper() == "RPSLS":
                OPTIONS = RPSLS
                WINS = RPSLS_WINS
                reply.append("You are now playing Rock, Paper, Scissors, Lizard, Spock\n")
            else:
                reply.append("Invalid game selection\n")

        print(f"Reply:{reply}")
        for r in reply:
            send_reply(conn=conn, type="RESP", msg=r)
        send_reply(conn=conn, type="END")

    print(f"Ending thread:{conn}")
    conn.close()


def server(interactive=True):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    print("Socket created")

    try:
        s.bind((HOST, PORT))
    except socket.error as e:
        print(f"Bind failed. Error: {e}")
        sys.exit()

    print("Socket bind complete")

    s.listen(10)
    print("Socket listening...")

    while True:
        client_socket, addr = s.accept()
        print(f"Connected with {addr[0]}:{str(addr[1])}")

        threading.Thread(target=client_thread, args=(client_socket, interactive,)).start()

    s.close()


if __name__ == "__main__":
    server()
