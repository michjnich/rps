import os
import socket

HOST = os.getenv("RPS_HOST", "localhost")
PORT = int(os.getenv("RPS_PORT", "8888"))

GAMES = {
    "RPS": ("Rock, Paper, Scissors", "Enter 'R'ock, 'P'aper or 'S'cissors to play."),
    "RPSLS": (
        "Rock, Paper, Scissors, Lizard, Spock",
        "Enter 'R'ock, 'P'aper, 'S'cissors, 'L'izard or Spoc'K' to play.",
    ),
}

GAME_SELECT = {
    1: "RPS",
    2: "RPSLS",
}

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.connect((HOST, PORT))

    exit_game = False
    game_type = "RPS"

    while not exit_game:
        msg_type = "CHOICE"
        qtext = GAMES.get(game_type, ["", "No game selected"])[1]
        qtext = qtext + " 'X' to exit, 'G'ame to swap game : "
        sdata = input(qtext)

        if sdata.upper() == "G":
            game_choice = 0
            msg_type = "GAME"
            while game_choice not in GAME_SELECT:
                for i, g in GAME_SELECT.items():
                    print(f"{i}. {GAMES[g][0]}")

                game_choice = int(input(f"Select game (1-{max(GAME_SELECT)}): "))
                if game_choice in GAME_SELECT:
                    game_type = GAME_SELECT[game_choice]
                    sdata = game_type

        if sdata.upper() == "X":
            msg_type = "EXIT"

        s.sendall(f"{len(sdata):<3}{msg_type:<8}{sdata}".encode())
        if sdata.upper() == "X":
            exit_game = True

        while True:
            data_length = int(s.recv(3).decode())
            resp_type = s.recv(8).decode().strip()
            if resp_type == "END":
                break
            data = s.recv(data_length).decode()
            print(data)
