import os
import asyncio
import websockets


HOST = os.getenv("RPS_HOST", "localhost")
PORT = int(os.getenv("RPS_PORT", "8888"))


async def process_message(websocket, path):
    msg = await websocket.recv()
    print(f"Incoming messgae:{msg}")


def webserver():
    start_server = websockets.serve(process_message, HOST, PORT)

    asyncio.get_event_loop().run_until_complete(start_server)
    asyncio.get_event_loop().run_forever()


if __name__ == "__main__":
    webserver()
